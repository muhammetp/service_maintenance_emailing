import psutil
import subprocess
import tempfile
import smtplib
from email.mime.text import MIMEText
import dateutil.parser
import re

process_name_list = ["cupsd","python3"]

def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


def display_all_process():
    # Iterate over all running process
    for proc in psutil.process_iter():
        try:
            # Get process name & pid from process object.
            processName = proc.name()
            processID = proc.pid
            print(processName , ' ::: ', processID)
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
        
        
def send_email(record):
    print("INSIDE MAIL")
    body = "body of mail " + record['alert'] +" end of mail"+" :\n"
    subject = record['type']+" Alert for Service "+record['service']
    sender = "s.kulkarni@mobily.com.sa"
    receiver = ["muhammet.pakyurek@ericsson.com"] #["s.vulligaddala@mobily.com.sa","s.kulkarni@mobily.com.sa"]
    #receiver=list_of_email_ids
    msg = MIMEText(body, 'html')
    msg['From'] = sender
    msg['To'] = ", ".join(receiver)
    msg['Subject']=subject
    print("mail body is:", body)
    print("mail sub is:", subject)
    print("Receviers are:",msg['To'])
    smtp_host_details="84.23.106.12"
    try:
        server = smtplib.SMTP(smtp_host_details,25)
        server.sendmail(sender,receiver,msg.as_string())
        print ("mail sent")
        success=1
    except Exception as e:
        print(e)
        success=0
    return success


def create_record_for_email(msg, err_type, service_name):
    return {"alert": msg, "type": err_type, "service": service_name}

def read_out_command_line(command):
    with tempfile.TemporaryFile() as o_tempf:
        proc = subprocess.Popen([command, 'a', 'b'], shell=True, stdout=o_tempf)
        proc.wait()
        o_tempf.seek(0)
        data = str(o_tempf.read(), 'utf-8')
    return data

def convert_to_datetime(s):
    return dateutil.parser.parse(s, fuzzy=True)


def remove_multiple_char(original_string, chars_to_remove):
    pattern = "[" + chars_to_remove + "]"
    return re.sub(pattern, "", original_string)


''' first task fo AI monitoring'''
def service_is_up_and_running():
    commands = ['systemctl status elasticsearch','systemctl status kibana', 'systemctl status logstash']
    matching_words = ['Active: ', 'inactive (dead)', 'active (running) since ']

    for command in commands:
        data = read_out_command_line(command)
        keyword = matching_words[0]
        n = data.find(keyword)
        if n > 0:
            activity_message = data[n:].split('\\n')[0]
            status = activity_message.split(" ")[1]

            service_name = command.split(" ")[2]
            err_type = status
            msg = activity_message
            record = create_record_for_email(msg, err_type, service_name)
            if status == 'failed':
                send_email(record)
            elif status == 'active':
                continue
            else:
                send_email(record)
        else:
            print("keyword not found", data)


date = None


def error_critical_exceptions_in_logs(file_paths : list):
    '''second task of Ai monitoring
    Error - critical exceptions occuring into  logs needs to be alarmed
    "/data/log/elasticsearch, elasticsearch-plain.log, python script we will send the mail for critical errors with tail or other command
    Find commands"
    '''
    delimiters = {"logstash" : "]", "elastic": " ", "kibana": " "}
    removed_chars = {"logstash": "[ ", "elastic": " ", "kibana": " "}
    linelength = {"logstash" : 2, "elastic": 2, "kibana": 2}
    services = ["logstash", "elastic", "kibana"]
    for file_path, service_name in zip(file_paths, services):
        command = "tail -n 100 " + file_path
        delimiter = delimiters[service_name]
        data = read_out_command_line(command)
        data = remove_multiple_char(data, removed_chars[service_name])
        data = data.split("\n")
        for line in data:
            keywords = line.split(delimiter)
            if len(keywords) > linelength[service_name]:
                date = convert_to_datetime(str(keywords[0]))
                msg_status = keywords[1]
                record = create_record_for_email(line, msg_status, service_name)
            if msg_status == 'error':
                send_email(record)


paths = ['/home/magneficent/elk_implementations/data_and_conf_files/log_files/logstash_log']
error_critical_exceptions_in_logs(paths)


